<?php
// So ova gi zapisuvam podatocina na mysql(using xampp)(Apache start, mysql start)
// http://localhost/phpmyadmin/index.php?route=/sql&db=vraboti&table=registration&pos=0
$host = 'localhost'; // Change to your MySQL host
$username = 'root'; // Change to your MySQL username (default is often 'root')
$password = ''; // Change to your MySQL password (default is often empty)
$database = 'vraboti'; //First add database to mysql 

// Create connection
$conn = new mysqli($host, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $fullname = htmlspecialchars($_POST["fullname"]);
    $company = htmlspecialchars($_POST["company"]);
    $email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
    $phone = htmlspecialchars($_POST["phone"]);
    $tip = htmlspecialchars($_POST["tip"]);

    // Insert data into the database
    $sql = "INSERT INTO registration (fullname, company, email, phone, tip) VALUES ('$fullname', '$company', '$email', '$phone', '$tip')";

    if ($conn->query($sql) === TRUE) {
        // Redirect to success page
        header('Location: http://127.0.0.1:5500/solutions/success.html');
        exit;
    } else {
        // Redirect to failed page
        header('Location: http://127.0.0.1:5500/solutions/failed.html');
        exit;
    }
}

$conn->close();
